﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class beatExtractor : MonoBehaviour 
{
	/**	
	 * Base: http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html#
	 * 
	 * @Mod: Aaron Effinger
	 * @Version: 2.0
	 * 
	 * 
	 * This class takes usses an audio listener to read music samples and output an array containing values for each frequency.
	 * 
	 * hz: 				array containing hz values
	 * pitchValue: 		Sound level in Hz
	 * 
	 * 
	**/	
	private int qSamples = 1024; 		//array size
	private float threshold = 0.02f; 	//minimum amplitude to extract pitch
	private AudioSource   aSource;
	private float[] samples;			//audio samples
	private float[] spectrum; 			//audio spectrum	
	
	public float refValue = 0.1f; 		//RMS value for 0 db
	public float rmsValue; 				//sound level - RMS
	public float dbValue;  				//sound level - db
	public float pitchValue; 			//sound level Hz
	public float[] band;
	public static float[] hz0;
	public float[] hzDebug0;
	public Text display; //used to show results for debug

			
	
	void Awake()
	{
		aSource = GetComponent<AudioSource>();

		samples = new float[qSamples];
		spectrum = new float[qSamples];

		int n  = spectrum.Length;

		// checks n is a power of 2 in 2's complement format
		if ((n & (n - 1)) != 0) {
			Debug.LogError ("spectrum length " + n + " is not a power of 2!!! Min: 64, Max: 8192.");
			return;
			
		}
		int k = 0;
		for(int j = 0; j < spectrum.Length; j++)
		{
			n = n / 2;
			if(n <= 0) break;
			k++;
		}


		band  = new float[k+1];
		hz0     = new float[k+1];
		for (int i = 0; i < band.Length; i++)
		{
			band[i] = 0;
			hz0[i] = 0;
		}
	}

	void Update()
	{
		hzDebug0 = hz0;
		
		AnalyzeSound ();
		if(display)
		{
			display.text = "RMS: "+rmsValue.ToString("f2")+
				" ("+dbValue.ToString("F1")+" db)\n"+
					"Pitch: "+pitchValue.ToString("F0")+" Hz";
		}
		
		Check();
	}

	/**
	 * update rms, db, and pitch average values.
	 * */
	void AnalyzeSound()
	{
		aSource.GetOutputData (samples, 0); //fill array with samples
		int i;
		float sum = 0;
		for (i=0; i<qSamples; i++) 
		{
			sum += samples[i]*samples[i]; //sum squared samples
		}
		rmsValue = Mathf.Sqrt (sum / qSamples); //rms = square root of average
		dbValue = 20 * Mathf.Log10 (rmsValue / refValue); //calculate db
		if (dbValue < -160)
			dbValue = -160; //set -160 as minimum value
		
		//get the sound spectrum
		aSource.GetSpectrumData (spectrum, 0, FFTWindow.Rectangular);
		float maxV = 0f;
		int maxN = 0;
		for(i=0; i < qSamples; i++) //find max 
		{
			if (spectrum[i] > maxV && spectrum[i] > threshold)
			{
				maxV = spectrum[i];
				maxN = i; //maxN is the index max
			}
		}
		float freqN = maxN; //pass the index to the float variable
		if(maxN > 0 && maxN < qSamples-1) // Interpolate index using neighbours
		{
			float dl = spectrum[maxN-1]/spectrum[maxN];
			float dr = spectrum[maxN+1]/spectrum[maxN];
			freqN += 0.5f*(dr*dr - dl*dl);
		}
		pitchValue = freqN*AudioSettings.outputSampleRate/qSamples; // convert index to frequency
	}
	
	
	/**
	 * Take currently playing song, grab a sample, and populate the hz array with values. 
	 * */
	public void Check()
	{

		int k = 0;
		int crossover = 2; 
		
		for (int i = 0; i < spectrum.Length; i++)
		{
			float d = spectrum[i];
			float b = band[k];
			// find the max as the peak value in that frequency band.
			band[k] = ( d > b )? d : b;
			if (i > (crossover-3) )
			{
				k++;
				crossover *= 2;   // frequency crossover point for each band.
				hz0[k] = band[k]*32;
				band[k] = 0;
			}
		}
	}

	public float[] getHZArrayC(){
		return hz0;
	}

	public float getCurrentHz(int point){
		if(point<=hz0.Length-1){
			return hz0[point];
		}
		return 0f;
	}
}