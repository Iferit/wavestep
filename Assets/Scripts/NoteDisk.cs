﻿using UnityEngine;
using System.Collections;

public class NoteDisk : MonoBehaviour {
	private float hz0;
	public float timeDie;
	private float timeAlive = 10f;
	public int point;
	public bool inishalized = false;
	private beatExtractor beat;
	private NoteSpawner notes;

	public float baseSize = 1f;
	public float maxSize = 2f;
	public float size = 1f;
	public float smothing = .2f;


	void Awake(){
		beat = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<beatExtractor>();
		notes = GameObject.FindGameObjectWithTag("GameController").GetComponent<NoteSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
		if (inishalized == true){
			hz0 = beat.getCurrentHz(point);
			timeDie -= Time.deltaTime+((hz0)/100);
			size = baseSize+(hz0*smothing);
			if(size > maxSize) size = maxSize+Random.Range(.1f,.5f);
			transform.localScale = new Vector3(size,size,size);

			if(timeDie<=0){
				die ();
			}
		}
		else{
			die ();
		}
	}

//	void OnCollisionEnter(Collision col){
//		if(col.gameObject.tag == "SoundDisk" && col.gameObject.GetComponent<NoteDisk>().size<this.size){
//			baseSize += .05f;
//			maxSize += .05f;
//			col.gameObject.GetComponent<NoteDisk>().die();
//		}
//	}

	public void inishalization(int p){
		beat.Check();
		point = p;
		hz0 = beat.getCurrentHz(p);

		timeDie = timeAlive;
		inishalized = true;
	}

	public void die(){
		notes.bandsUsed--;
		Destroy(gameObject);
	}

	public float getHZ(){
		return hz0;
	}
}
