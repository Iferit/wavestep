﻿using UnityEngine;
using System.Collections;

public enum states{mainMenue, playing, paused, end, songSelection}

public class State : MonoBehaviour {
	public static State state;
	public static states gameState;
	public AudioClip[] songs;
	public AudioClip song;

	public GameObject playGUI;
	public GameObject menuGUI;
	public GameObject songSelectGUI;
	public GameObject gameController;
	public GameObject audioManager;
	public GameObject player;

	void Awake(){
		DontDestroyOnLoad(transform.gameObject);
		if(state){
			Destroy(this.gameObject);
		}else{
			state = this;
		}
	}


	void Start () {
		gameState = states.mainMenue;
		checkState();
	}

	public void checkState(){
		switch(gameState){

		case states.mainMenue:
			songSelectGUI.SetActive(false);
			menuGUI.SetActive(true);
			playGUI.SetActive(false);
			GetComponent<NoteSpawner>().enabled = false;
			audioManager.GetComponent<AudioSource>().Stop();
			player.SetActive(false);
			break;

		case states.playing:
			songSelectGUI.SetActive(false);
			menuGUI.SetActive(false);
			playGUI.SetActive(true);
			GetComponent<NoteSpawner>().enabled = true;
			audioManager.GetComponent<AudioSource>().Play();
			player.SetActive(true);
			break;

		case states.paused:
			songSelectGUI.SetActive(false);
			menuGUI.SetActive(false);
			playGUI.SetActive(true);
			GetComponent<NoteSpawner>().enabled = false;
			audioManager.GetComponent<AudioSource>().Pause();
			player.SetActive(false);
			break;

		case states.songSelection:
			songSelectGUI.SetActive(true);
			menuGUI.SetActive(false);
			playGUI.SetActive(false);
			GetComponent<NoteSpawner>().enabled = false;
			audioManager.GetComponent<AudioSource>().Stop();
			player.SetActive(false);
			break;

		case states.end:
			Application.Quit();
			break;

		default:
			changeState("mainMenue");
			break;
		}
	}

	public void changeState(string changerTo){
			switch(changerTo){
				
			case "mainMenue":
			gameState = states.mainMenue;
				break;
				
			case "playing":
			gameState = states.playing;
			audioManager.GetComponent<AudioSource>().clip = song;
				break;
				
			case "paused":
			gameState = states.paused;
				break;
				
			case "songSelection":
			gameState = states.songSelection;
				break;
				
			case "end":
			gameState = states.end;
				break;
				
			default:
			gameState = states.mainMenue;
				break;
			}
		checkState();
		}

	public void songToPlay(int sNum){
		switch(sNum){
		case 0:
			song = songs[0];
			break;
		case 1:
			song = songs[1];
			break;
		case 2:
			song = songs[2];
			break;
		}
	}
}
