﻿using UnityEngine;
using System.Collections;

public class HzMove : MonoBehaviour 
{
	public float timeBetween = 1;
	public float speedM;
	public float timeM = .5f;
	public float smooth = 2;

	private GameObject audioManager;
	private AudioAnalizerc audioAnz;
	private AudioRead reader;
	private Vector3 newPos;
	private Vector3 pos1;
	private Vector3 pos2;
	private Vector3 pos3;
	private Vector3 pos4;



	void Start () 
	{
		audioManager = GameObject.FindGameObjectWithTag ("GameController");
		audioAnz = audioManager.GetComponent<AudioAnalizerc>();
		reader = audioManager.GetComponent<AudioRead>();
		audioAnz.Check();

	}
	void Update()
	{
		timeBetween -= Time.deltaTime;
		if (timeBetween<0)
		{
			UpdatePos();
			timeBetween = reader.rmsValue/timeM;
		}

	}
	public void UpdatePos()
	{
		audioAnz.Check();
		for(int i = 1; i<7;i++)
		{
			Points points = GameObject.Find("Point_"+i).GetComponent<Points>();
			PointMove curGO = GameObject.Find("Point_"+i).GetComponent<PointMove>();
			float curHz = audioAnz.hz[i];
			Vector3 pos1 = GameObject.Find("node_0"+i+"_01").transform.position;
			Vector3 pos2 = GameObject.Find("node_0"+i+"_02").transform.position;
			Vector3 pos3 = GameObject.Find("node_0"+i+"_03").transform.position;
			Vector3 pos4 = GameObject.Find("node_0"+i+"_04").transform.position;
			if (curHz<1)
			{
				newPos = pos1;
				points.color = Color.gray;
				points.rank = 1;
			}
			else if(curHz>=1 && curHz<2)
			{
				newPos = pos2;
				points.color = Color.blue;
				points.rank = 2;
			}
			else if(curHz>=2 && curHz<3)
			{
				newPos = pos3;
				points.color = Color.yellow;
				points.rank = 3;
			}
			else if(curHz>=3)
			{
				newPos = pos4;
				points.color = Color.red;
				points.rank = 4;
			}
			//float smoothing = reader.rmsValue*smooth;
			// will use a for look later to find all positions on each update.
			
			points.id = i;
			curGO.moveTo = newPos;
			curGO.smooth = smooth;
		}

	}
}
