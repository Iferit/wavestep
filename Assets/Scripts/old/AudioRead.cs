﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioRead : MonoBehaviour 
{
	//Base: 
	//http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html#

	public int qSamples = 1024; 		//array size
	public float refValue = 0.1f; 		//RMS value for 0 db
	public float threshold = 0.02f; 	//minimum amplitude to extract pitch
	public float rmsValue; 				//sound level - RMS
	public float dbValue;  				//sound level - db
	public float pitchValue; 			//sound level Hz
	public float smothingVal;

	private float[] samples;			//audio samples
	public float[] spectrum; 			//audio spectrum
	// private float fSample;

	void Start()
	{
		samples = new float[qSamples];
		spectrum = new float[qSamples];
		// fSample = AudioSettings.outputSampleRate;

		InvokeRepeating("SmothingUpdate", 0.0f, 1.0f/30.0f); // update at 30 fps
	}

	void AnalyzeSound()
	{
		GetComponent<AudioSource>().GetOutputData (samples, 0); //fill array with samples
		int i;
		float sum = 0;
		for (i=0; i<qSamples; i++) 
		{
			sum += samples[i]*samples[i]; //sum squared samples
		}
		rmsValue = Mathf.Sqrt (sum / qSamples); //rms = square root of average
		dbValue = 20 * Mathf.Log10 (rmsValue / refValue); //calculate db
		if (dbValue < -160)
						dbValue = -160; //set -160 as minimum value

		//get the sound spectrum
		GetComponent<AudioSource>().GetSpectrumData (spectrum, 0, FFTWindow.Rectangular);
		float maxV = 0f;
		int maxN = 0;
		for(i=0; i < qSamples; i++) //find max 
		{
			if (spectrum[i] > maxV && spectrum[i] > threshold)
			{
				maxV = spectrum[i];
				maxN = i; //maxN is the index max
			}
		}
		float freqN = maxN; //pass the index to the float variable
		if(maxN > 0 && maxN < qSamples-1) // Interpolate index using neighbours
		{
			float dl = spectrum[maxN-1]/spectrum[maxN];
			float dr = spectrum[maxN+1]/spectrum[maxN];
			freqN += 0.5f*(dr*dr - dl*dl);
		}
		pitchValue = freqN*AudioSettings.outputSampleRate/qSamples; // convert index to frequency
	}

	public Text display; //used to show results for debug

	void Update()
	{
		AnalyzeSound ();
		if(display)
		{
			display.text = "RMS: "+rmsValue.ToString("f2")+
				" ("+dbValue.ToString("F1")+" db)\n"+
					"Pitch: "+pitchValue.ToString("F0")+" Hz";
		}
	}
	void SmothingUpdate()
	{
		smothingVal = rmsValue;
	}
}
