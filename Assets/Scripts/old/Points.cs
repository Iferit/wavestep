﻿using UnityEngine;
using System.Collections;

public class Points : MonoBehaviour 
{
	public int points;
	public int id;
	public Color color;
	public int rank;

	private float scale = 1f;
	private AudioAnalizerc audioAn;
	private float hz= 0f;
	Player player;

	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		rank = 0;
		audioAn = GameObject.FindGameObjectWithTag("GameController").GetComponent<AudioAnalizerc>();
		points = 0;
		color = Color.gray;
	}

	void Update () 
	{
		/*
		switch(hz)
		{
		case hz < .5f:
			scale = .5f;
			break;
		case hz > 3f:
			scale = 3f;
			break;
		default:
			scale = hz;
		}
		*/
		if(hz<.5f)
		{
			scale=.5f;
		}
		else if(hz>1.5f)
		{
			scale = 1.5f;
		}
		else
		{
			scale = hz;
		}

		if (id != 0)
		{
			hz = audioAn.hz[id];
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(scale,scale,scale), Time.deltaTime*2);
			GetComponent<Renderer>().material.color = Color.Lerp(GetComponent<Renderer>().material.color, color, 2 * Time.deltaTime);
		}
	}
	void OnTriggerStay(Collider other)
	{
		Debug.Log ("hit!");
		int incScore = 0;
		switch(rank)
		{
		case 1:
			incScore = 1;
				break;
		case 2:
			incScore = 3;
			break;
		case 3:
			incScore = 5;
				break;
		case 4:
			incScore = 10;
			break;
		default:
			incScore = 0;
			break;
		}
		if (other.gameObject.tag == "Player")
		{
			player.score += incScore;
			Debug.Log("Added "+incScore.ToString() + " points to score. score is now " + player.score);
		}

	}
}
