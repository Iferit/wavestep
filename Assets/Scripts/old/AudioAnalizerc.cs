﻿using UnityEngine;
using System.Collections;

public class AudioAnalizerc : MonoBehaviour 
{
	public int numOfSamples = 1024; //Min: 64, Max: 8192
	public AudioSource   aSource;
    public float[] freqData;
    public float[] band;
	public float[] hz;
	AudioRead reader;
	
    void Start () 
    {
		reader = GameObject.FindWithTag("GameController").GetComponent<AudioRead>();
        freqData = new float[numOfSamples];
        int n  = freqData.Length;
        // checks n is a power of 2 in 2's complement format
        if ((n & (n - 1)) != 0) {
            Debug.LogError ("freqData length " + n + " is not a power of 2!!! Min: 64, Max: 8192.");
            return;

        }
        int k = 0;
        for(int j = 0; j < freqData.Length; j++)
        {
            n = n / 2;
            if(n <= 0) break;
            k++;
        }
        band  = new float[k+1];
        hz     = new float[k+1];
        for (int i = 0; i < band.Length; i++)
        {
            band[i] = 0;
            hz[i] = 0;
        }
        InvokeRepeating("Check", 0.0f, 1.0f/30.0f); // update at 30 fps
    }
    public void Check()
    {
        freqData = reader.spectrum;
        int k = 0;
        int crossover = 2; 

        for (int i = 0; i < freqData.Length; i++)
        {
            float d = freqData[i];
            float b = band[k];
            // find the max as the peak value in that frequency band.
            band[k] = ( d > b )? d : b;
            if (i > (crossover-3) )
            {
                k++;
                crossover *= 2;   // frequency crossover point for each band.
				hz[k] = band[k]*32;
                band[k] = 0;
            }
        }
    }
    void Update () 
	{

	}
}
