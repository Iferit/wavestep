﻿using UnityEngine;
using System.Collections;

public class PointMove : MonoBehaviour {
	public Vector3 moveTo;
	public float smooth = 2f;

	void Start()
	{
		moveTo = transform.position;
	}
	void Update () {
		if (transform.position != moveTo)
		{
			transform.position = Vector3.Lerp(transform.position, moveTo, Time.deltaTime*smooth);
		}
	}
}
