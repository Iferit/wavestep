﻿using UnityEngine;
using System.Collections;

public class Lights : MonoBehaviour 
{
	public AudioRead reader;

	// Use this for initialization
	void Start () {
		reader = GameObject.FindGameObjectWithTag("GameController").GetComponent<AudioRead>();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Light>().intensity = reader.dbValue*.3f;
	}
}
