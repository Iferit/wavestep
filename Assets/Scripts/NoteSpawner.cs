﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NoteSpawner : MonoBehaviour {
	private beatExtractor beat;
	public  float top;
	public  float bottom;
	public  float left;
	public  float right;
	private float positionVert;

	//public float nextSpawnTime; //used to text spawn
	public float spawnFrequency = 1f;
	public float spawnFLimit = .1f;
	public float noteSpawnTalerance = 1f;

	public GameObject notePrefab;
	public float[] timer;
	public int bandLimit = 2;
	public int bandsUsed = 0;
	public float timeAdder; //amount of time to ajust spawns based on rms
	public int[] debugOrderBands;
	public float rmsMax;
	public bool spawnside = true;

	public float doSpawnNext;
	public float doSpawnTime = 3f;


	void Start(){
		doSpawnNext = doSpawnTime;
		beat = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<beatExtractor>(); 
		timer = new float[beat.band.Length-1];
		for(int i = 0; i<timer.Length;i++){
			timer[i]=0;
		}
		findBorders();
		//nextSpawnTime = spawnFrequency;
		bandLimit = 11;
		bandsUsed = 0;
		positionVert = (Math.Abs(top)+ Math.Abs(bottom))/timer.Length;

	}

	void FixedUpdate(){
		/* 
		// for testing spawn
		nextSpawnTime-=Time.deltaTime;
		if(nextSpawnTime < 0){
			float posx = Random.Range(left,right);
			float posy = Random.Range(top,bottom);
			spawnNote(posx,posy,Random.Range(0,11));
			nextSpawnTime = spawnFrequency;
		}
		*/

		tickTimmer();
		float timeToAdd = beat.rmsValue+Time.deltaTime;
		if(timeToAdd>spawnFLimit){
			timeToAdd = spawnFLimit;
		}
		doSpawnNext -= timeToAdd;
		if(doSpawnNext<0){
			doSpawning();
			doSpawnNext = doSpawnTime;
		}
		if(beat.rmsValue>rmsMax)
			rmsMax = beat.rmsValue;
	}

	public void findBorders(){
		Vector3 topL = GameObject.FindGameObjectWithTag("TopLeft").transform.position;
		Vector3 botR = GameObject.FindGameObjectWithTag("BotRight").transform.position;
		
		top = topL.y;
		bottom = botR.y;
		left = topL.x;
		right = botR.x;
	}

	public void spawnNote(float posX, float posY, int point){
		GameObject note = Instantiate(notePrefab,new Vector3(posX,posY,0),new Quaternion(0,0,0,0)) as GameObject;
		note.GetComponent<NoteDisk>().inishalization(point);
		bandsUsed++;
	}

	public void doSpawning(){
		int[] list = getOrderBands();
		debugOrderBands=list;
		bool spawned = false;
		for(int i =0; i<bandLimit;i++){
			if(bandsUsed<bandLimit){
				float side;
				side = beat.rmsValue*5;
				if (side > right) side = (right);
				if(timer[list[i]] < 0 && beat.getCurrentHz(list[i])>noteSpawnTalerance){
					if(spawnside){
						spawnside = false;
					}else{
						side = 0-side;
						spawnside = true;
					}
					spawnNote(side,(top-(positionVert*list[i])-1),list[i]);
					timer[list[i]] = spawnFrequency;
					spawned = true;
					return;
				}
			}

		}
		if (spawned == false && beat.getCurrentHz(list[0]) > noteSpawnTalerance){
			float side;
			side = beat.rmsValue*5;
			if(spawnside){
				spawnside = false;
			}else{
				side = 0-side;
				spawnside = true;
			}
			spawnNote(side,(top-(positionVert*list[0])-1),list[0]);
			timer[list[0]] = spawnFrequency;
			spawned = true;
		}
	}
	//returns an array of bands, in order from most active to least.
	public int[] getOrderBands(){

		float[] hz;
		int size = beat.getHZArrayC().Length;
		hz = new float[size];

		float[] hzT0 = beat.getHZArrayC();

		for (int i = 0;i<size;i++){
			hz[i] = hzT0[i];
		}
		int[] orderList = new int[bandLimit];

		for(int i = 0; i<bandLimit;i++){
			float bigest = 0;
			if(i==0){
				for(int k = 0;k<size;k++){
					if(hz[k]>bigest){
						bigest = hz[k];
						orderList[i] = k;
					}
				}
			}else{
				for(int k = 0;k<size;k++){
					if(hz[k]>bigest && hz[k]<hz[orderList[i-1]]){
						bigest = hz[k];
						orderList[i] = k;
					}
				}
			}
		}
		return orderList;
	}

	public void tickTimmer(){
		for(int i = 0; i<timer.Length; i++){
			timer[i] -= Time.deltaTime;
		}
	}

	public void determinMaxBands(){

	}

	}
