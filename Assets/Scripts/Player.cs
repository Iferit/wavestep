﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour 
{
	public float score;
	public Vector3 playerPos;
	public int fingerCount = 0;
	public float speed = 0.02f;

	public Text display; //used to show results for debug
	void Start()
	{
		score = 0;
	}
	void FixedUpdate()
	{
		PlayerPos();
		display.text = score.ToString();
	}

	void PlayerPos() //Map the players possition to the first finger press
	{
		if (Input.touchCount > 0) 
		{
			Touch touch = Input.GetTouch(0); //grab the first touch info
			//check if the phase is stationary, or moved
			if (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Began)
			{
				//if touching check to see if the renderer and collider are enabled, if not enable them
//			if (GetComponent<Renderer>().enabled == false)
//					GetComponent<Renderer>().enabled = true;
			if (GetComponent<Collider>().enabled == false)
					GetComponent<Collider>().enabled = true;
			if(GetComponent<Light>().enabled == false)
					GetComponent<Light>().enabled = true;

				float xPos = -3.6f + 7.2f * touch.position.x / Screen.width;
				float yPos = -5.8f + 11.6f * touch.position.y / Screen.height;
				//set the touch possision to a vector 3
				//Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 0));          
				//set the possision of this object to the new possision
				//transform.position = touchPosition;
				transform.position = new Vector3(xPos,yPos,0);
				
			}
		}
		else if(Input.GetMouseButton(0))
		{

			//check if the phase is stationary, or moved

				//if touching check to see if the renderer and collider are enabled, if not enable them
				//if (GetComponent<Renderer>().enabled == false)
				//	GetComponent<Renderer>().enabled = true;
				if (GetComponent<Collider>().enabled == false)
					GetComponent<Collider>().enabled = true;
				if(GetComponent<Light>().enabled == false)
					GetComponent<Light>().enabled = true;
				//set the touch possision to a vector 3
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);          
				//set the possision of this object to the new possision
				transform.position = new Vector3(mousePos.x, mousePos.y, 0);
		}
		else 
		{
			//if no touch disable renderer and collider
			//if (GetComponent<Renderer>().enabled == true)
			//		GetComponent<Renderer>().enabled = false;
			if (GetComponent<Collider>().enabled == true)
					GetComponent<Collider>().enabled = false;
			if(GetComponent<Light>().enabled == true)
				GetComponent<Light>().enabled = false;
			//set the possision off screen
			transform.position = new Vector3(0, 25, 0);
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag=="SoundDisk"){
			if (Input.touchCount > 0) 
			{
				if(Input.GetTouch(0).phase == TouchPhase.Began){
					score += other.GetComponent<NoteDisk>().getHZ ();
					other.GetComponent<NoteDisk>().die();
				}else{
					score += other.GetComponent<NoteDisk>().getHZ ()/3;
					other.GetComponent<NoteDisk>().die();
				}
			}
		}
	}

}